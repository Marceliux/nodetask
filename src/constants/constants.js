const estadosValidos = {
    values: ['PENDIENTE', 'AGENDADA', 'CANCELADA', 'COMPLETADA'],
    message: '{VALUE} no es un rol valido'
}

module.exports = estadosValidos